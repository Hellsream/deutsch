\select@language {english}
\contentsline {chapter}{\numberline {1}Handbuch}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Freemind}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Vorwort}{3}{subsection.1.1.1}
\contentsline {subsubsection}{Was ist FreeMind und wozu nutze ich es?}{3}{subsubsection*.3}
\contentsline {subsection}{\numberline {1.1.2}Grundlegende Funktionen}{4}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Knoten}{5}{subsection.1.1.3}
\contentsline {subsubsection}{Neues Projekt erstellen}{5}{subsubsection*.4}
\contentsline {subsubsection}{Knoten erstellen}{5}{subsubsection*.5}
\contentsline {subsubsection}{Knotentext bearbeiten}{5}{subsubsection*.6}
\contentsline {subsubsection}{Knoten l\IeC {\"o}schen}{5}{subsubsection*.7}
\contentsline {subsubsection}{Bestimmte Knoten suchen}{6}{subsubsection*.8}
\contentsline {subsubsection}{Grafische Verbindungen einf\IeC {\"u}gen}{6}{subsubsection*.9}
\contentsline {subsection}{\numberline {1.1.4}Icons}{7}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.1.5}Knoten manipulieren}{7}{subsection.1.1.5}
\contentsline {subsubsection}{Format \IeC {\"a}ndern}{7}{subsubsection*.10}
\contentsline {subsubsection}{Stile}{8}{subsubsection*.11}
\contentsline {subsection}{\numberline {1.1.6}Schlusswort}{8}{subsection.1.1.6}
