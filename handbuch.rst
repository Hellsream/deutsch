
########
Handbuch
########
********
Freemind
********
.. image:: .\Free_Mind.jpg
    :align: center

**Von Jonathan Gröger**



Vorwort
=======


Was ist FreeMind und wozu nutze ich es?
---------------------------------------


FreeMind ist ein kostenloses Open Source Programm zum erstellen von Mindmaps. Dies kann in vielen Situationen hilfreich sein, wie z.B.: Projektmanagement oder zum Strukturieren von Ideen. Zudem kann man unkompliziert Bilder und Texte einfügen.

.. image:: .\MindMap.jpg
*So könnte zum Beispiel eine fertige Mindmap aussehen.*

Ein Vorteil bei FreeMind ist, dass es plattformunabhängig ist, da es in Java programmiert ist.

Grundlegende Funktionen
=======================
Die am dringendsten Funktionen sind gleich in der Menüleiste unter dem Punkt Datei zu finden.

.. image:: .\Menueleiste.jpg

Hier kann man ein neues Projekt erstellen, ein bereits erstelltes Projekt öffnen oder das aktuelle Projekt speichern. Hier kann man ebenfalls das aktuelle Projekt drucken.

Knoten
======
Neues Projekt erstellen
-----------------------
Zum Erstellen eines neuen Projekts klickt man unter 'Datei' auf Neu.

Am besten speichert man jetzt gleich die neue Mindmap unter 'Datei'-> 'Speichern unter'. Hier jetzt noch den Speicherort wählen und einen Dateinamen vergeben.

Knoten erstellen
----------------
Um einen neuen Unterknoten zu erstellen, klickt man entweder im Werkzeugmenü auf das Lampenzeichen oder geht den Weg über 'Einfügen'->'Neuer Unterknoten'.
Der Unterknoten wird erstellt und man ist gleich in der Zeile, an der der Name eingegeben werden soll.

Knotentext bearbeiten
---------------------
Um den Knotennamen zu ändern macht man einfach einen Doppelklick auf den gewünschten Knoten. Wenn der Name dann wie gewünscht geändert ist einfach auf einen anderen Knoten klicken oder Enter drücken.

Knoten löschen
--------------
Knoten kann man löschen, indem man sie auswählt (anklickt) und auf die Entfernen-Taste auf der Tastatur drück. Alternativ dazu kann man auch einen Rechtsklick auf den Knoten machen und Knoten löschen auswählen.

Bestimmte Knoten suchen
-----------------------
Um nach einem bestimmten Knoten in der MindMap zu suchen drückt man Strg und F gleichzeitig. Hier gibt man den Namen des gesuchten Knoten ein und drückt dann Enter.

ACHTUNG: Es werden immer nur Unterknoten von dem Knoten durchsucht, den man gerade angewählt hat.

Grafische Verbindungen einfügen
-------------------------------
.. image:: .\Verbindung.jpg
Um solche Verbindungen zu erstellen muss man zwei Knoten auswählen. Dazu einfach Strg gedrückt halten und die gewünschten Knoten anklicken. Dann Strg und L gleichzeitig drücken.
Der Pfeil geht immer vom zuerst ausgewählten Knoten aus.

Mit einem Rechtsklick auf den Pfeil erscheint ein Menü zum ändern der Pfeilrichtungen, der Farbe oder zum löschen.

.. image:: .\Menue_Pfeil.jpg
Icons
=====
.. image:: .\Symbolleiste.jpg
    :align: left
Durch die Symbolleiste am linken Rand kann man bei dem Knoten, der gerade angewählt ist, verschiedene Symbole hinzufügen, wie z.B.: Ziffern, Pfeile oder Smileys.

Über das rote Kreuz Icon ganz oben in der Leiste kann man erstellte Symbole wieder löschen. Ebenso kann man mit dem Mülltonnen-Symbol entfernt man alle Icons auf einmal.

Icons kann man auch über die Menüleiste unter dem Punkt Einfügen einsetzen oder mit einem Rechtsklick auf dem gewünschten Knoten.

Knoten manipulieren
===================
Format ändern
--------------
Bei Knoten kann man die Farbe, Schriftart, Schriftgröße und vieles mehr auch ändern. Mit einem Rechtsklick auf den gewünschten Knoten unter dem Punkt 'Format' sieht man alle Möglichkeiten.

.. image:: .\Format.jpg
Stile
-----
Stile sind dazu da Knoten einheitlich für bestimmte Sachen zu markieren, z.B.: als Standardknoten oder wenn er noch zu bearbeiten ist

Es gibt einige vorgefertigte Stile. Diese Findet man durch einen Rechtsklick auf den Knoten unter 'Stile'.

Allerdings kann man auch eigene Stile erstellen. Dazu drückt man F11. Hier ändert man die schon existerenden Stile oder erstellt neue.

Schlusswort
===========
Dies ist nur ein kleiner Überblick über die wichtigsten Funktionen in FreeMind.
FreeMind eignet sich sehr gut für Einsteiger, da alle Menüpunkte sehr selbsterklärend sind.
