.. Handbuch Freemind documentation master file, created by
   sphinx-quickstart on Fri Nov 27 08:32:27 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Handbuch Freemind's documentation!
=============================================

Inhaltsverzeichnis


.. toctree::
   :maxdepth: 2

   handbuch.rst
